import 'dart:developer';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:aws_ivs_broadcast/aws_ivs_broadcast.dart';

class BroadCastWidget extends StatefulWidget {
  const BroadCastWidget({
    super.key,
  });

  @override
  State<BroadCastWidget> createState() => _NewBroadCastWidgetState();
}

class _NewBroadCastWidgetState extends State<BroadCastWidget> {
  late AwsIvsBroadcastController _ivsController;
  late CameraController _cameraController;

  bool _cameraInit = false;

  @override
  void initState() {
    super.initState();

    _setup();
  }

  Future<void> _setup() async {
    final cameras = await availableCameras();
    _cameraController = CameraController(cameras[1], ResolutionPreset.medium);
    await _cameraController.initialize();


    _ivsController = AwsIvsBroadcastController();
    _ivsController.stateController.stream.listen((event) {
      log(event.name.toString());
    });

    await _ivsController.init();

    _cameraInit = true;
    setState(() {});
  }

  @override
  void dispose() {
    _cameraController.dispose();
    _ivsController.dispose();

    super.dispose();
  }

  String key = "...";
  String url = "rtmps://...";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              onPressed: () async {
                await _ivsController.startBroadcast(key, url);
              },
              child: const Text('Start Broadcast'),
            ),
            ElevatedButton(
              onPressed: () async {
                await _ivsController.stopBroadcast();
              },
              child: const Text('Stop Broadcast'),
            ),
          ],
        ),
      ),
      body: _cameraInit ?  _ivsController.previeWidget(): Container(),
    );
  }
}
