import 'package:pigeon/pigeon.dart';

@ConfigurePigeon(
  PigeonOptions(
    dartOut: 'lib/src/channel_api.g.dart',
    javaOut:
        'android/src/main/java/com/aws_utils/ivs_broadcast/aws_ivs_broadcast/ChannelApi.java',
    javaOptions: JavaOptions(
      package: 'com.aws_utils.ivs_broadcast.aws_ivs_broadcast',
    ),
  ),
)
enum AwsIvsBroadCastState {
  invalid,
  disconnected,
  connecting,
  connected,
  error;
}

@HostApi()
abstract class AwsIvsBroadcastChannelApi {
  void init();
  void dispose();
  int getTextureId();
  void startBroadcast(String streamKey, String ingestServer);
  void stopBroadcast();
}

@FlutterApi()
abstract class AwsIvsBroadcastChannelCallback {
  void onStateChanged(AwsIvsBroadCastState state);
}
