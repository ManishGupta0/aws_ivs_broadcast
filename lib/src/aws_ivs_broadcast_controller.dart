import 'dart:async';

import 'package:aws_ivs_broadcast/src/channel_api.g.dart';
import 'package:flutter/material.dart';

class AwsIvsBroadcastController extends AwsIvsBroadcastChannelCallback {
  AwsIvsBroadcastController() : _api = AwsIvsBroadcastChannelApi();

  final AwsIvsBroadcastChannelApi _api;

  bool _init = false;
  int _textureId = -1;

  final stateController = StreamController<AwsIvsBroadCastState>.broadcast();

  Future<void> init() async {
    await _api.init();
    _textureId = await _api.getTextureId();
    _init = true;
  }

  Future<void> dispose() async {
    await _api.dispose();
  }

  Future<void> startBroadcast(String streamKey, String ingestServer) async {
    await _api.startBroadcast(streamKey, ingestServer);
  }

  Future<void> stopBroadcast() async {
    await _api.stopBroadcast();
  }

  Widget previeWidget() {
    if (_init) {
      return Texture(textureId: _textureId);
    }

    return const Center(
      child: Text(
        'Controller not initialized',
        style: TextStyle(fontSize: 24),
      ),
    );

    // throw Exception("Controller not initialized");
  }

  @override
  void onStateChanged(AwsIvsBroadCastState state) {
    stateController.add(state);
  }
}
