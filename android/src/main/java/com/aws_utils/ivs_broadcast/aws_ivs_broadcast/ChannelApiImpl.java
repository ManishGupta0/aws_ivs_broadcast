package com.aws_utils.ivs_broadcast.aws_ivs_broadcast;

import static com.aws_utils.ivs_broadcast.aws_ivs_broadcast.AwsIvsBroadcastPlugin.TAG;

import android.app.Activity;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.amazonaws.ivs.broadcast.BroadcastConfiguration;
import com.amazonaws.ivs.broadcast.BroadcastException;
import com.amazonaws.ivs.broadcast.BroadcastSession;
import com.amazonaws.ivs.broadcast.Device;
import com.amazonaws.ivs.broadcast.ImageDevice;
import com.amazonaws.ivs.broadcast.ImagePreviewView;
import com.amazonaws.ivs.broadcast.Presets;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.view.TextureRegistry;

public class ChannelApiImpl implements ChannelApi.IvsBroadcastChannelApi, PlatformView {
    private final LinearLayout layout;
//    private final Activity activity;
//    private final FlutterPlugin.FlutterPluginBinding binding;

    private ChannelApi.IvsBroadcastChannelCallback callbacks;

    static public TextureRegistry.SurfaceTextureEntry textureEntry;

    private Context ctx;

    private BroadcastSession broadcastSession;

    private BroadcastSession.Listener broadcastListener = new BroadcastSession.Listener() {
        @Override
        public void onStateChanged(@NonNull BroadcastSession.State state) {
            Log.d(TAG, "State=" + state);
            // sendEvent(state.name());
        }

        @Override
        public void onError(BroadcastException exception) {
            Log.e(TAG, "Exception: " + exception);
        }
    };

    public ChannelApiImpl(Context context, FlutterPlugin.FlutterPluginBinding flutterPluginBinding){
//        this.activity = activity;
//        this.binding = flutterPluginBinding;
        this.ctx = context;
        this.callbacks = new ChannelApi.IvsBroadcastChannelCallback(flutterPluginBinding.getBinaryMessenger());

        layout = new LinearLayout(this.ctx);
    }

    private void sendEvent(String event) {
        callbacks.onStateChanged(ChannelApi.IvsBroadCastState.valueOf(event), null);
    }

    @Override
    public Long getTextureId() {
        if(textureEntry == null){
            return 0l;
        }
        return textureEntry.id();
    }

    @Override
    public void init() {
       
    }
    @Override
    public void startBroadcast(@NonNull String streamKey, @NonNull String ingestServer) {
         this.broadcastSession = new BroadcastSession(
            this.ctx,
            this.broadcastListener,
            Presets.Configuration.STANDARD_PORTRAIT,
            Presets.Devices.FRONT_CAMERA(this.ctx)
        );

        this.broadcastSession.start(ingestServer, streamKey);

        this.broadcastSession.awaitDeviceChanges(() -> {
                    Log.e(TAG, "awaitDeviceChanges");
                    for(Device device: this.broadcastSession.listAttachedDevices()) {
                        // Find the camera we attached earlier
                        if(device.getDescriptor().type == Device.Descriptor.DeviceType.CAMERA) {
                            View view = getView();
                            assert view != null;
                            ImagePreviewView preview = ((ImageDevice) device)
                                    .getPreviewView(BroadcastConfiguration.AspectMode.FILL);
                            preview.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.MATCH_PARENT));
                            layout.addView(preview);
                        }
                    }
                }
        );

        // ImagePreviewView preview = ((ImageDevice)device).getPreviewView(BroadcastConfiguration.AspectMode.FILL);
//        ImagePreviewView preview = broadcastSession.getPreviewView();
//        Log.e(TAG, "preview = " + preview);

//        SurfaceTexture t = preview.getSurfaceTexture();

//        preview.setSurfaceTextureListener(
//            new TextureView.SurfaceTextureListener() {
//                public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
////                    Log.e(TAG, "texture = " + t);
//                    textureEntry = binding.getTextureRegistry().registerSurfaceTexture(surface);
//                }
//
//                public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
//                }
//
//                public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
//                    return false;
//                }
//
//                public void onSurfaceTextureUpdated(SurfaceTexture surface) {
//                }
//            }
//        );



        
    }

    @Override
    public void stopBroadcase() {
        if(this.broadcastSession.isReady()){
            this.broadcastSession.stop();
        }

        this.broadcastSession.release();
    }

    @Nullable
    @Override
    public View getView() {
        return layout;
    }

    @Override
    public void dispose() {
        stopBroadcase();
    }
}
