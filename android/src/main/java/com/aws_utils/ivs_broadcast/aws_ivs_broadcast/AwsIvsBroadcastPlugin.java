package com.aws_utils.ivs_broadcast.aws_ivs_broadcast;

import android.util.Log;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;

/** AwsIvsBroadcastPlugin */
public class AwsIvsBroadcastPlugin implements FlutterPlugin, ActivityAware {

  public final static String TAG = "IVS_Broadcast";
  private FlutterPluginBinding flutterPluginBinding;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
    this.flutterPluginBinding = binding;
    Log.i(TAG, "onAttachedToEngine");
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    this.flutterPluginBinding = null;
    Log.i(TAG, "onDetachedFromEngine");
  }

  @Override
  public void onAttachedToActivity(@NonNull ActivityPluginBinding binding) {
    Log.i(TAG, "onAttachedToActivity");

    this.flutterPluginBinding.getPlatformViewRegistry().registerViewFactory(
            "aws_ivs_broadcast_view",
            new ChannelFactory(this.flutterPluginBinding)
    );
  }

  @Override
  public void onDetachedFromActivity() {
    Log.i(TAG, "onDetachedFromActivity");
  }

  @Override
  public void onReattachedToActivityForConfigChanges(@NonNull ActivityPluginBinding binding) {
    onAttachedToActivity(binding);
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
    onDetachedFromActivity();
  }
}
