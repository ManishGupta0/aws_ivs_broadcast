package com.aws_utils.ivs_broadcast.aws_ivs_broadcast;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MessageCodec;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

public class ChannelFactory extends PlatformViewFactory {
    private FlutterPlugin.FlutterPluginBinding flutterPluginBinding;

    public ChannelFactory(FlutterPlugin.FlutterPluginBinding binding) {
        super(StandardMessageCodec.INSTANCE);
        this.flutterPluginBinding = binding;
    }

    @NonNull
    @Override
    public PlatformView create(Context context, int viewId, @Nullable Object args) {
        ChannelApiImpl channel = new ChannelApiImpl(context, this.flutterPluginBinding);

        ChannelApi.IvsBroadcastChannelApi.setUp(
                this.flutterPluginBinding.getBinaryMessenger(),
                channel
        );
        return channel;
    }
}
